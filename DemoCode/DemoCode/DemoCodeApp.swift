//
//  DemoCodeApp.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import SwiftUI

@main
struct DemoCodeApp: App {
    var body: some Scene {
        WindowGroup {
            SplashScreenView()
        }
    }
}
