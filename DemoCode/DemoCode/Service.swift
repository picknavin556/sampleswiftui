//
//  Service.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 21/12/2564 BE.
//

import UIKit

struct MyError:Error {
    var message:String
}

class Service {
    static let share = Service()
    
    
    
    
    func fetchRequestPeople(handleCallBack:@escaping([String:Any]?, MyError?)->Void){
        
        let ROOT_URL = "https://swapi.dev/api/"
        let strURL = ROOT_URL + "people"
        
        let url = URL(string: strURL)
        
    
        guard let url = url else {
            // if null
            let error = MyError(message: "URL wrong")
            handleCallBack(nil, error)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest) { data, resoinse, error in
            // request finish
            
            if let error = error {
                print(error.localizedDescription)
                let e = MyError(message: error.localizedDescription)
                handleCallBack(nil, e)
                return
            }
            
            guard let data = data else {
                print("Something went wrong")
                let e = MyError(message: "Something went wrong")
                handleCallBack(nil, e)
                return
            }
            
            do{
                let jsonData:[String:Any]? = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                
                guard let jsonData = jsonData else {
                    let str = String(data: data, encoding: .utf8)
                    print(str ?? "Can't encode string")
                    let e = MyError(message: "Can't encode string")
                    handleCallBack(nil, e)
                    return
                }
                
                print("Success!!")
                print(jsonData)
                
                handleCallBack(jsonData, nil)
                
            }catch{
                print(error.localizedDescription)
                return
            }
            
            
        }
        
        task.resume()
        
    }
    
    
    
    
    func fetchLogin(id:String, password:String, handleCallBack:@escaping([String:Any]?, MyError?)->Void){
        
        let ROOT_URL = "https://dev-mobile.mrfox.app/api"
        let strURL = ROOT_URL + "/Auth/login"
        
        let url = URL(string: strURL)
        
    
        guard let url = url else {
            // if null
            let error = MyError(message: "URL wrong")
            handleCallBack(nil, error)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        // Set header
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-type")
        
        
        //set Body
        var body:[String:Any] = [String:Any]()
        body["username"] = id
        body["password"] = password
        body["deviceID"] = "54759f80fdf7052f81f1beae1b74a09916c780846d664bdecd2aef4222c31af3"
        body["oneSignalID"] = "063c48e5-1ec4-4ebd-b302-937aceb32f8f"
        
        let jsonBody = try? JSONSerialization.data(withJSONObject: body)
        urlRequest.httpBody = jsonBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest) { data, resoinse, error in
            // request finish
            
            if let error = error {
                print(error.localizedDescription)
                let e = MyError(message: error.localizedDescription)
                handleCallBack(nil, e)
                return
            }
            
            guard let data = data else {
                print("Something went wrong")
                let e = MyError(message: "Something went wrong")
                handleCallBack(nil, e)
                return
            }
            
            do{
                let jsonData:[String:Any]? = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                
                guard let jsonData = jsonData else {
                    let str = String(data: data, encoding: .utf8)
                    print(str ?? "Can't encode string")
                    let e = MyError(message: "Can't encode string")
                    handleCallBack(nil, e)
                    return
                }
                
                
                if let e = self.checkError(json: jsonData) {
                    handleCallBack(nil, e)
                    return
                }
                
                print("Success!!")
                print(jsonData)
                
                handleCallBack(jsonData, nil)
                
            }catch{
                print(error.localizedDescription)
                return
            }
            
            
        }
        
        task.resume()
        
        
        
        
    }
    
    
    
    func checkError(json:[String:Any])->MyError?{
        
        if let success = json["success"] as? Bool {
            if(success == true){
                return nil
            }
        }
            
        if let message = json["message"] as? String {
            return MyError(message: message)
        }else{
            return MyError(message: "Something went wrong")
        }
        
    }
}
