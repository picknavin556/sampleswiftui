//
//  LoginViewController.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 21/12/2564 BE.
//

import UIKit

class LoginViewController: ObservableObject {

  

    
    func loginWith(id:String, password:String, complet:@escaping(MyError?)->Void){
        Service.share.fetchLogin(id: id, password: password) { result, error in
            
            if let e = error {
                complet(e)
                return
            }
            print(result)
            guard let result = result else {
                complet(MyError(message: "result null"))
                return
            }
            
     
            ShareData.share.userLogin = Login(json: result)
            complet(nil)
        }
    }
    
    
    func isLoginSuccess()->Bool{
      
        guard let login = ShareData.share.userLogin else {
            return false
        }
        
        if(login.userId != ""){
            return true
        }else{
            return false
        }
    }

}
