//
//  SplashScreenViewController.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 22/12/2564 BE.
//

import UIKit

class SplashScreenViewController: ObservableObject {
    var timer:Timer?
    
    var callback:()->Void = {() in }
    
    func startTimmer(handle:@escaping ()->Void){
        
        callback = handle
        if let _ = timer { return }
        
        timer = Timer.scheduledTimer(timeInterval:10, target: self, selector: #selector(timerAction), userInfo: nil, repeats: false)
    }
    
    @objc func timerAction(){
        self.callback()
        
        if let t = timer {
            if(t.isValid){
                t.invalidate()
            }
            timer = nil
        }
        
    }
    
}
