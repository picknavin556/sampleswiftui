//
//  CharacterListViewControl.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import Foundation

class CharacterListViewControl: ObservableObject {
    
    var arCharacter:[CharacterModel] = [CharacterModel]()
    
 
    
    
    
    init(){
        print("init CharacterListViewControl")
        
    }
  
    func getPeopleList(complet:@escaping()->Void) {
        arCharacter.removeAll()
        Service.share.fetchRequestPeople(handleCallBack: { jsonData, error in
            
            if let e = error {
                print(e)
                return
            }
            
            if let jsonData = jsonData {
                if let results = jsonData["results"] as? [[String: Any]] {
                    
                    for item in results {
                        let newCharacter = CharacterModel(json: item)
                        self.arCharacter.append(newCharacter)
                        
                    }
                    
                }
            }
            
            complet()
        })
    }
}
