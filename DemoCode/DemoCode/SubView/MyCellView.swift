//
//  MyCellView.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import SwiftUI

struct MyCellView: View {
    @State var title:String
    @State var imageURL:String
    @State var tag:Int = 0
    var body: some View {
        HStack(){
            AsyncImage(url: URL(string: imageURL)){ image in
                image
                    .resizable()
                    .scaledToFit()
                
            } placeholder: {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
            }
            .frame(width: 60, height: 80, alignment: .center)
            
            
            Text(title)
            
            
           
            
        }
        .frame( maxWidth: .infinity, minHeight: 90, alignment: .leading)
        .padding()
        
    }
}

struct MyCellView_Previews: PreviewProvider {
    static var previews: some View {
        MyCellView(title: "title", imageURL: "https://picsum.photos/200/300")
            .previewLayout(.sizeThatFits)
    }
}
