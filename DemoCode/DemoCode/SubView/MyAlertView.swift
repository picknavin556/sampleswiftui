//
//  MyAlertView.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import SwiftUI

protocol MyAlertViewDelegate {
    
    func myAlertOK()
    func myAlertCancel()
    
}

struct MyAlertView: View {
    
    @State var title:String
    @State var detail:String
    
    @Binding var showAlert:Bool
    
    @State var delegate:MyAlertViewDelegate? = nil
    
    var body: some View {
        ZStack(){
            VStack(){
                Text(title)
                    .padding()
                
                Text(detail)
                    .padding()
                
                
                HStack(){
                    Button("OK") {
                        tapOnOK()
                    }
                    .frame(maxWidth: .infinity)
                    
                    
                    Button("Cancel") {
                        tapOnCancel()
                    }
                    .frame(maxWidth: .infinity)
                    
                }
                
            }
            .frame(minWidth: 250, idealWidth: 250, maxWidth: 250, alignment: .center)
            .background(Color.white)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.primary.opacity(0.35))
        
        
     
    }
    
    func tapOnOK(){
        
        if let de = delegate {
            de.myAlertOK()
        }
        closeAlert()
    }
    
    func tapOnCancel(){
        
        if let de = delegate {
            de.myAlertCancel()
        }
        
        closeAlert()
    }
    func closeAlert(){
        showAlert = false
    }
}

struct MyAlertView_Previews: PreviewProvider {
    
    
    static var previews: some View {
        PreviewErapper()
            .previewLayout(.sizeThatFits)
    }
    
    struct PreviewErapper: View {
        @State var showAlert:Bool = true
        
        var body: some View {
            MyAlertView(title: "Title", detail: "detail", showAlert: $showAlert)
                
        }
    }
}


