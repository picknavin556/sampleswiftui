//
//  Login.swift
//  thai-super-phone-ios
//
//  Created by Supapon Pucknavin on 23/11/2564 BE.
//

import Foundation

struct Login: Hashable, Codable {
    var hashableId = UUID()
    
    var userId:String = ""
    var username:String = ""
    var roleId:Int = -1
    var roleName:String = ""
    var token:String = ""
    var audience:String = ""
    var issuer:String = ""
    var issueAt:Date = Date()
    var expiresIn:Int = -1
    var expireDate:Date = Date()
    var tokenFoxclub:String = ""
    var profileUUID:String = ""
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(hashableId)
    }
    
    
    init(json: [String:Any]) {
        
        if let data = json["data"] as? [String:Any] {
            readJsonData(json: data)
        }else{
            readJsonData(json: json)
        }
        
    }
    
    mutating func readJsonData(json:[String: Any]) {

        userId = stringValue(map: json, key: "userId")
        username = stringValue(map: json, key: "username")
        roleId = intValue(map: json, key: "roleId")
        roleName = stringValue(map: json, key: "roleName")
        token = stringValue(map: json, key: "token")
        audience = stringValue(map: json, key: "audience")
        issuer = stringValue(map: json, key: "issuer")
        issueAt = dateValue(map: json, key: "issueAt")
        expiresIn = intValue(map: json, key: "expiresIn")
        expireDate = dateValue(map: json, key: "expireDate")
        tokenFoxclub = stringValue(map: json, key: "tokenFoxclub")
        profileUUID = stringValue(map: json, key: "profileUUID")
 
        
    }
    
  
  
}
