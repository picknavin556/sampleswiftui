//
//  CharacterModel.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import Foundation

struct CharacterModel: Hashable {
    var hashableID = UUID()
    
    
    var imageURL:String = ""
    var name:String = ""
    var height:String = ""
    var mass:String = ""
    var hair_color:String = ""
    var skin_color:String = ""
    var eye_color:String = ""
    var birth_year:String = ""
    var gender:String = ""
    var homeworld:String = ""
    var films:[String] = [String]()
   
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(hashableID)
    }
    
    
    init(json:[String:Any]){
        readJSON(json: json)
    }
    
    mutating func readJSON(json:[String:Any]){
        
        
        if let value = json["name"] as? String {
            name = value
        }
        
        if let value = json["height"] as? String {
            height = value
        }
        
        if let value = json["mass"] as? String {
            mass = value
        }
        
        if let value = json["hair_color"] as? String {
            hair_color = value
        }
        
        if let value = json["skin_color"] as? String {
            skin_color = value
        }
        
        if let value = json["eye_color"] as? String {
            eye_color = value
        }
        
        if let value = json["birth_year"] as? String {
            birth_year = value
        }
        
        if let value = json["gender"] as? String {
            gender = value
        }
        
        if let value = json["homeworld"] as? String {
            homeworld = value
        }
        
        if let value = json["films"] as? [String] {
            films = value
        }
        
    }
}
