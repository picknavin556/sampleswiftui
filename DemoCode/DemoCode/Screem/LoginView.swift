//
//  LoginView.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 21/12/2564 BE.
//

import SwiftUI

struct LoginView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    
    @State var userId:String = ""
    @State var password:String = ""
    @State var loading:Bool = false
    @State var isError:Bool = false
    @State var errorMessage:String = ""
    @Binding var activeModalView:SplashScreenView.ModolView?
    @ObservedObject var controller:LoginViewController = LoginViewController()
    
    var body: some View {
        ZStack() {
            
            ScrollView(){
                VStack(alignment: .center) {
                    
                    Spacer()
                        .frame(width: .infinity, height: 60, alignment: .center)
                    
                    
                    TextField("User Name", text: $userId)
                        .padding()
                    
                    SecureField("Password", text: $password)
                        .padding()
                    
                    
                    
                    Button("Login") {
                        tapOnLogin()
                    }
                    .foregroundColor(.red)
                    .frame(width: 180, height: 44, alignment: .center)
                    .background(.green)
                    
                    
                    Button("close") {
                        exitScreen()
                    }
                    
                }
            }
            
            
            if(loading){
                VStack(alignment: .center) {
                    
                    Text("Loading...")
                }
                .frame(width: 320, height: 320, alignment: .center)
                .background(Color.purple)
                .edgesIgnoringSafeArea(.all)
            }
        }
        .onAppear {
            setupView()
        }
        .alert(errorMessage, isPresented: $isError) {
            Button("OK", role: .cancel) {
                // do someting
                
            }
        }
        
    }
    
    
    func setupView() {
        
    }
    
    func tapOnLogin() {
        UIApplication.shared.endEditing()
        loading = true
        print("user Id: \(userId)")
        print("Password: \(password)")
        controller.loginWith(id: userId, password: password) { error in
            
            loading = false
            
            
            if let e = error {
                isError = true
                errorMessage = e.message
            }
            
            if(controller.isLoginSuccess()){
                isError = true
                errorMessage = "Login success!!"
            }
            
            
        }
        
    }
    
    
    func exitScreen(){
//        presentationMode.wrappedValue.dismiss()
        activeModalView = .menu
    }
}

struct LoginView_Previews: PreviewProvider {
    
    static var previews: some View {
        PreviewErapper()
            .previewLayout(.sizeThatFits)
    }
    
    struct PreviewErapper: View {
       
        @State var activeModalView:SplashScreenView.ModolView? = .login
        var body: some View {
            LoginView(activeModalView: $activeModalView )
                
        }
    }
    
   
}
