//
//  CharacterListView.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import SwiftUI

struct CharacterListView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var controll:CharacterListViewControl = CharacterListViewControl()
    
    
    @State var arData:[CharacterModel] = [CharacterModel]()
    
    
    var body: some View {
        ZStack(){
            Color.cyan
            
            
            VStack(){
                
                List(){
                    ForEach(arData, id:\.self) { item in
                        
                        Button {
                            print("Button was tapped ")
                        } label: {
                            MyCellView(title: item.name, imageURL: item.imageURL)
                        }
                        .listRowSeparator(.hidden)
                    }
                    .onDelete(perform: self.deleteRow)
                    

                }
                .listStyle(.plain)
                .edgesIgnoringSafeArea(.all)
                
                
                
                
                Button("reload data") {
                    reloadData()
                }
                
                
                Button("close") {
                    closeView()
                }
                
            }
        }
        .onAppear(){
            reloadData()
        }
        
        
        
        
    }
   
    func closeView(){
        presentationMode.wrappedValue.dismiss()
    }
    
    func reloadData(){
        controll.getPeopleList(complet: {
            arData = controll.arCharacter
        })
        
    }
    
    
    func deleteRow(at indexSet: IndexSet) {
        print(indexSet)
        self.arData.remove(atOffsets: indexSet)

    }
}

struct CharacterListView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterListView()
    }
}
