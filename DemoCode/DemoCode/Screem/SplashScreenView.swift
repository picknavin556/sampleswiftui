//
//  SplashScreenView.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 22/12/2564 BE.
//

import SwiftUI

struct SplashScreenView: View {
    
    enum ModolView: String, Identifiable {
        case menu
        case login
        var id:String {
            return self.rawValue
        }
    }
    
    @State var activeModalView:ModolView? = nil
    
    @ObservedObject var controller:SplashScreenViewController = SplashScreenViewController()
    
    var body: some View {
        
        
        ZStack(){
            
            
        
            
            VStack{
                Image("dog6")
                    .resizable()
                    .scaledToFill()
            }
            
            
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear {
//            UIView.setAnimationsEnabled(false)
            
            print("onAppear")
            startTimmer()
        }
        .fullScreenCover(item: $activeModalView) {
            activeModalView = nil
        } content: { contentModal in
            if(contentModal == .menu){
               ContentView()
            }else if(contentModal == .login){
                LoginView( activeModalView: $activeModalView)
            }
        }
    }
    
    
    func startTimmer() {
        if let _ = ShareData.share.userLogin{
            activeModalView = .menu
        }else{
            activeModalView = .login
        }
    }
    
    
}

struct SplashScreenView_Previews: PreviewProvider {
    static var previews: some View {
        SplashScreenView()
    }
}
