//
//  ContentView.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 20/12/2564 BE.
//

import SwiftUI

struct ContentView: View {
    
    enum ModolView: String, Identifiable {
        case character
        case detail
        var id:String {
            return self.rawValue
        }
    }
    
    @State var actionSheetModalView:Bool = false
    @State var activeModalView:ModolView? = nil
    @State var showAlert:Bool = false
    
    @State var activeLogin:SplashScreenView.ModolView? = nil
    init(){
       
        
    }
    
    var body: some View {
        NavigationView() {
            ZStack(){
                
                VStack(){
                    
                    NavigationLink("Goto Character with navigation") {
                        CharacterListView()
                    }
                    
                    
                    
                    Button("go to Action sheet") {
                        showCharacterView()
                    }
                    .sheet(isPresented: $actionSheetModalView) {
                        CharacterListView()
                    }
                    
                    
                    Button("go to Modal full screen") {
                        showModalFullScreen()
                    }
                    
                    

                    
                    
                    
                    Button("Show Alert") {
                        showAlert = true
                    }
                    
                    
                    
                    NavigationLink("Login") {
                        LoginView(activeModalView: $activeLogin)
                    }
                   
                }
                
                
                
                
                
                if(showAlert == true) {
                    MyAlertView(title: "MyTitle", detail: "MyDetail...", showAlert: $showAlert, delegate: self)
                }
                    
                
            }
            
            
            .navigationBarTitle("Navigation bar Title")
        }
        .onAppear(){
            print("== init ==")
            print(ShareData.share.userLogin?.userId)
            
            
        }
        .fullScreenCover(item: $activeModalView) {
            activeModalView = nil
        } content: { contentModal in
            if(contentModal == .character){
                CharacterListView()
            }else if(contentModal == .detail){
                Text("detail")
            }
        }

        
        
        
        
        
        
        
    }
    
    
    func showCharacterView() {
        print("showCharacterView")
        actionSheetModalView = true
    }
    
    
    func showModalFullScreen(){
        activeModalView = .character
        
    }
    
}

extension ContentView: MyAlertViewDelegate {
    func myAlertOK() {
        print("myAlertOK")
    }
    
    func myAlertCancel() {
        print("myAlertCancel")
    }
    
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
