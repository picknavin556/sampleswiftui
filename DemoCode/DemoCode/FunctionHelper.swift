//
//  Helper.swift
//  thai-super-phone-ios
//
//  Created by Supapon Pucknavin on 23/11/2564 BE.
//

import Foundation


func stringValue(map:[String:Any], key:String)->String {
    
    if let value = map[key] as? String {
        return value
    }
    
    if let value = map[key] as? Int {
        return "\(value)"
    }
    
    if let value = map[key] as? Double {
        return "\(value)"
    }
    
    if let value = map[key] as? Bool {
        return "\(value)"
    }
    
    return ""
}


func intValue(map:[String:Any], key:String) -> Int {
    if let item = map[key] as? Int {
        return item
    }
    
    if let item = map[key] as? String {
        return Int(item) ?? 0
    }
    return 0
}

func doubleValue(map:[String:Any], key:String) -> Double {
    if let item = map[key] as? Double {
        return item
    }
    if let item = map[key] as? String {
        return Double(item) ?? 0
    }
    return 0
}

func boolValue(map:[String:Any], key:String) -> Bool {
    if let item = map[key] as? Bool {
        return item
    }
    return false
}



func dateValue(map:[String:Any], key:String)->Date {
    
    guard let value = map[key] as? String else {
        return Date()
    }
    
    if let date = value.detectDate {
        return date
    }
    return Date()
}

/*
 func getAllFont(){
 for family in UIFont.familyNames.sorted() {
 let names = UIFont.fontNames(forFamilyName: family)
 print("Family: \(family) Font names: \(names)")
 }
 }
 */

func getUUID() -> String {
    let user = UserDefaults.standard
    if user.value(forKey: "uuid") == nil {
        let uuid = NSUUID().uuidString.lowercased()
        user.set(uuid, forKey: "uuid")
        user.synchronize()
    }
    
    return user.value(forKey: "uuid") as! String
}

func machineName() -> String {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    return machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
    }
}

func getVersionMyApp(withShowBuild isShowBuild: Bool) -> String {
    
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    
    if isShowBuild == true {
        
        return "\(version) build \(build)"
    }else {
        
        return "\(version)"
    }
}

func getOSInfo()->String {
    let os = ProcessInfo().operatingSystemVersion
    return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
}


func timeStringHHmmss(sec:Double)->String{
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.hour, .minute, .second]
    formatter.unitsStyle = .positional

    let formattedString = formatter.string(from: TimeInterval(sec))!
    return formattedString
}
