//
//  Extention.swift
//  DemoCode
//
//  Created by Supapon Pucknavin on 21/12/2564 BE.
//

import Foundation
import SwiftUI



extension String {
    var detectDate: Date? {
        let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.date.rawValue)
        let matches = detector?.matches(in: self, options: [], range: NSMakeRange(0, self.utf16.count))
        return matches?.first?.date
    }
    func index(from: Int) -> Index {
            return self.index(startIndex, offsetBy: from)
        }
    func substring(with r: Range<Int>) -> String {
            let startIndex = index(from: r.lowerBound)
            let endIndex = index(from: r.upperBound)
            return String(self[startIndex..<endIndex])
        }
}



extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
